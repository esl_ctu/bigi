#!/bin/bash

reset
cd ..
./build.sh -c

echo ""
echo ""
echo "    Running bigi examples"
echo "================================================================================"
echo ""

./bin/desktop_example | ruby

echo ""
